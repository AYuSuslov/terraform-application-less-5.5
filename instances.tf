data "google_compute_image" "debian_image" {
  family  = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_instance" "vm_instance_lb" {
  count        = var.node_count
  name         = "vm-instance-${count.index}"
  machine_type = "e2-medium"
  tags         = ["terraform-application"]
  labels       = {
    env = "terraform-app"
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian_image.self_link
    }
  }
}

resource "null_resource" "first_host_to_hostfile" {
  provisioner "local-exec" {
    command = "echo ${google_compute_instance.vm_instance_lb.0.name} ${google_compute_instance.vm_instance_lb.0.network_interface.0.access_config.0.nat_ip} > host.list"
  }
}

resource "null_resource" "second_host_to_hostfile" {
  triggers = {
    order = null_resource.first_host_to_hostfile.id
  }

  provisioner "local-exec" {
    command = "echo ${google_compute_instance.vm_instance_lb.1.name} ${google_compute_instance.vm_instance_lb.1.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  provisioner "local-exec" {
    working_dir = "deploy_ansible"
    command = "ansible-playbook playbook_deploy_appl.yml"
  }
}

