resource "google_compute_instance_group" "tf_lb_instances" {
  name      = "lb-instances-group"
  zone      = var.gcp_zone
  instances = "${google_compute_instance.vm_instance_lb.*.id}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_region_backend_service" "tf_lb_backends" {
  name          = "lb-backend-service"
  region        = var.gcp_region
  load_balancing_scheme = "EXTERNAL"
  health_checks = [google_compute_region_health_check.tf_lb_hc.id]
  backend {
    group       = google_compute_instance_group.tf_lb_instances.id
  }
}

resource "google_compute_region_health_check" "tf_lb_hc" {
  name    = "lb-health-check"
  region  = var.gcp_region
  tcp_health_check {
    port  = "8000"
  }
}

resource "google_compute_forwarding_rule" "tf_lb_rule" {
  name            = "lb-forwarding-rule"
  region          = var.gcp_region
  ip_address      = google_compute_address.tf_lb_addr.id
  ports           = ["8000"]
  backend_service = google_compute_region_backend_service.tf_lb_backends.id
}

resource "google_compute_address" "tf_lb_addr" {
  name = "lb-address"  
}

resource "cloudflare_record" "juneway" {
  zone_id = var.cloudflare_zone_id
  name = "ayususlov.juneway.pro"
  value = google_compute_address.tf_lb_addr.address
  type = "A"
  ttl = 3600
}
