output "Hostname" {
  value = google_compute_instance.vm_instance_lb.*.name
}

output "Host_IP" {
  value = google_compute_instance.vm_instance_lb.*.network_interface.0.access_config.0.nat_ip
}


output "GCP_Project_ID" {
  value = google_compute_instance.vm_instance_lb.0.project
}

output "Loadbalancer_global_IP" {
  value = google_compute_address.tf_lb_addr.address
}
