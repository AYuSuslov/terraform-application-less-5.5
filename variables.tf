variable "gcp_project" {
  default = "carbide-cairn-296118"
}
variable "gcp_region" {
  default = "us-west3"
}
variable "gcp_zone" {
  default = "us-west3-c"
}
variable "gcp_creds" {}
variable "cloudflare_email" {}
variable "cloudflare_api_key" {}
variable "cloudflare_zone_id" {}
variable "node_count" {
  default = "2"
}
