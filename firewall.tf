resource "google_compute_firewall" "http" {
  name    = "terraform-application"
  network = "default"

  dynamic "allow" {
    for_each = ["8000", "8001"]
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["terraform-application"]
}
